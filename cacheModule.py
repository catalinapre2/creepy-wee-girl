import time
import logging

from google.appengine.api import memcache
from dbmodels import *

def updateCache(entry, isNew):
	key = 'last10'
	mc = memcache.Client()
	info = mc.gets(key)
	if info == None:
	    memcache.add(key=key, value=([entry], time.time()))
	    return
	if isNew:
		try:
			entries, updated = info
			entries.insert(0,entry)
			entries = entries[:10]
		except:
			entries = list(db.GqlQuery("SELECT * FROM Entry ORDER BY timestamp DESC limit 10"))
	else:
		try:
			entries, updated = info
			for e in entries:
				if e.Key().id() == entry.Key().id():
					e = entry
		except:
			entries = list(db.GqlQuery("SELECT * FROM Entry ORDER BY timestamp DESC limit 10"))
	updated = time.time()
	done = mc.cas(key, (entries,updated))
	while done == False:
		info = mc.gets(key)
		logging.error(info)
		try:
			entries, updated = info
		except:
			entries = list(db.GqlQuery("SELECT * FROM Entry ORDER BY timestamp DESC limit 10"))
		updated = time.time()
		done = mc.cas(key, (entries, updated))		    

def cachePost(entry):
	postID = str(entry.key().id())
	cacheTime = time.time()
	info = (entry, cacheTime)
	memcache.set(postID, info)			

def get_last10_age():
	key = "last10"
	cache = memcache.get(key)
	if cache == None:
		last10 = list(db.GqlQuery("SELECT * FROM Entry ORDER BY timestamp DESC limit 10"))
		timestamp = time.time()
		memcache.set(key, (last10, timestamp))
	else:
		last10, timestamp = cache
	age = time.time() - timestamp
	return last10, age
