import os
import webapp2
import jinja2
import re
import string
import hashlib
import hmac
import random

from google.appengine.ext import db
from utils import *
from dbmodels import *

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

SECRET = 'Iwywhwm7dy'
def hash_str(s):
    return hmac.new(SECRET, s).hexdigest()

def make_secure_val(s):
    return "%s|%s" % (s, hash_str(s))

def check_secure_val(h):
    if h:
    	val = h.split('|')[0]
    else:
	return None
    if h == make_secure_val(val):
        return val

def make_salt():
    return ''.join(random.choice(string.letters) for x in xrange(5))

def make_pw_salt_hash(name, pw, salt = None):
    if not salt:
	salt = make_salt()
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s,%s' % (h, salt)

def valid_pw(name, pw, h):
    salt = h.split(',')[1]
    return h == make_pw_salt_hash(name, pw, salt)

def verify_username(s):
	return re.match(r"^[a-zA-Z0-9_-]{3,20}$", s)

def verify_duplicate_username(un):
	entries = db.GqlQuery("Select * From UserEntries Where username = '%s'" % un)
	if entries.count() > 0 :
	    return True
	else:
	    return False
	
def verify_password(s):
	return re.match(r"^.{3,20}$", s)
	
def verify_email(s):
	return re.match(r"^[\S]+@[\S]+\.[\S]+$", s) or s == ''

def get_username_from_id(id_str):
	key = db.Key.from_path('UserEntries', int(id_str))#, parent = blog_key())
	user = db.get(key)	
	if not user:
		return None 
	return user.username

class WelcomePage(Handler):
	def get(self):
		user_id_hash = self.request.cookies.get('user_id')
		if user_id_hash:
			if check_secure_val(user_id_hash):
				self.render("welcome.html", username = get_username_from_id(user_id_hash.split('|')[0]))
		else:
			self.redirect("/signin")

class SignupPage(Handler):
	def write_form(self, username='', username_error='', duplicate_username_error='', password_error='', verify_error='', email='', email_error=''):
		self.response.headers.add_header("charset", "utf-8")
		self.render("signup.html", username = username, 
							username_error = username_error, 
							duplicate_username_error = duplicate_username_error,
							password_error = password_error, 
							verify_error = verify_error, 
							email = email, 
							email_error = email_error)
		
	def get(self):
		self.write_form()
		
	def post(self):
		user_username = str(self.request.get('username'))
		user_password = str(self.request.get('password'))
		user_verify = str(self.request.get('verify'))
		user_email = str(self.request.get('email'))
		
		username_ok = verify_username(user_username)
		username_exists = verify_duplicate_username(user_username)
		password_ok = verify_password(user_password)
		verify_ok = (user_password == user_verify)
		email_ok = verify_email(user_email)
		
		username_error = ''
		duplicate_username_error = ''
		password_error = ''
		verify_error = ''
		email_error = ''
		
		if not username_ok:
			username_error = "That's not a valid username."
		if username_exists:
			duplicate_username_error = "This username already exists."
		if not password_ok:
			password_error = "That wasn't a valid password."
		if not verify_ok:
			verify_error = "Your passwords didn't match"
		if not email_ok:
			email_error = "That's not a valid email."
		
		if not (username_ok and not username_exists and password_ok and verify_ok and email_ok):
			self.write_form(user_username, username_error, duplicate_username_error, password_error, verify_error, user_email, email_error)

		else:
			a = UserEntries(username = user_username, password = make_pw_salt_hash(user_username, user_password), email = user_email)
			a.put()
			user_id = str(a.key().id())
			cookie_content = make_secure_val(user_id)
			self.response.headers.add_header('Set-Cookie', "user_id=%s; Path=/" % cookie_content)
			self.redirect("/welcome")

class SigninPage(Handler):
	def write_form(self, username='', username_error='', password_error=''):
		self.response.headers.add_header("charset", "utf-8")
		self.render("signin.html", username = username, 
							username_error = username_error, 
							password_error = password_error)
		
	def get(self):
		self.write_form()
		
	def post(self):
		user_username = str(self.request.get('username'))
		user_password = str(self.request.get('password'))
		
		username_exists = verify_duplicate_username(user_username)
		password_ok = False
		
		username_error = ''
		password_error = ''
		
		if not username_exists:
			username_error = "This username does not exist."
		else:
			entries = db.GqlQuery("Select * From UserEntries Where username = '%s'" % user_username)
			entry = entries.get()
			password_ok = valid_pw(user_username, user_password, entry.password)
		if not password_ok and username_exists:
			password_error = "That was a wrong password."
		
		if not (username_exists and password_ok):
			self.write_form(user_username, username_error, password_error)
		else:
			
			user_id = str(entry.key().id())
			cookie_content = make_secure_val(user_id)
			self.response.headers.add_header('Set-Cookie', "user_id=%s; Path=/" % cookie_content)
			self.redirect("/welcome")
	
class LogoutPage(Handler):
	def get(self):
		cookie_content = ""
		self.response.headers.add_header('Set-Cookie', "user_id=%s; Path=/" % cookie_content)
		self.redirect("/")


	
app = webapp2.WSGIApplication([('/signup/?', SignupPage), ('/welcome/?', WelcomePage), ('/signin/?', SigninPage), ('/logout/?', LogoutPage)], debug = True)
