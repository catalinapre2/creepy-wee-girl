from google.appengine.ext import db

class Entry(db.Model):
	subject = db.StringProperty(required = True)
	content = db.TextProperty(required = True)
	timestamp = db.DateTimeProperty(auto_now_add = True)

	def render(self, template):
		self._render_text = self.content
		self.render(template, p = self)

class UserEntries(db.Model):
	username = db.StringProperty(required = True)
	password = db.StringProperty(required = True)
	email = db.StringProperty(required = False)
	
	def render(self, template):
		self._render_text = self.content.replace('\n','<br>')
		self.render(template, p = self)
