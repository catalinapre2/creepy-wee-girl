import logging
import time

from utils import *
from cacheModule import *
from useraccounts import *
from dbmodels import *

class PostNew(Handler):
    def get(self):
        user_id_hash = self.request.cookies.get('user_id')
        if check_secure_val(user_id_hash):
            username = get_username_from_id(user_id_hash.split('|')[0])
            if(username == "Creepy"):
                self.render("newentry.html")
            else: 
                self.redirect("/")
        else:
            self.redirect("/signin")
        
        
    def post(self):
        subject = self.request.get("subject")
        content = self.request.get("content")
        
        if subject and content:
            a = Entry(subject = subject, content = content)
            a.put()
            updateCache(a, True)
            cachePost(a)
            newpostID = str(a.key().id())
            self.redirect("/%s"%newpostID)
            
        else:
            error = "We need both a title and an entry!"
            self.render("newentry.html", subject=subject, content=content, error=error)
            
class OnePost(Handler):
    def get(self, postID):
        if memcache.get(postID) == None:
            key = db.Key.from_path('Entry', int(postID))
            post = db.get(key)
            postTime = time.time()
            memcache.set(postID, (post, postTime))
        (post, postTime) = memcache.get(postID)
        age = time.time() - postTime
        if not post:
            self.error(404)
            return    
        user_id_hash = self.request.cookies.get('user_id')
        if check_secure_val(user_id_hash):
            username = get_username_from_id(user_id_hash.split('|')[0])
        else:
            username = None    
        if username == 'Creepy':
            edit = postID
        else:
            edit = None        
        self.render('singlepost.html', e=post, age=age, user=username, edit=edit)

class EditPost(Handler):
    def get(self, postID):
        user_id_hash = self.request.cookies.get('user_id')
        if check_secure_val(user_id_hash):
            username = get_username_from_id(user_id_hash.split('|')[0])
            if(username == "Creepy"):
                if memcache.get(postID) == None:
                    key = db.Key.from_path('Entry', int(postID))
                    post = db.get(key)
                    postTime = time.time()
                    memcache.set(postID, (post, postTime))
                (post, postTime) = memcache.get(postID)
                if not post:
                    self.error(404)
                    return                    
                self.render('newentry.html', subject=post.subject, content=post.content)
            else:
                self.redirect("/")
        else:

            self.redirect("/signin")

    def post(self, postID):
        subject = self.request.get("subject")
        content = self.request.get("content")
        if subject and content:
            key = db.Key.from_path('Entry', int(postID))
            a = db.get(key)
            a.subject = subject
            a.content = content
            a.put()
            updateCache(a, False)
            cachePost(a)
            self.redirect("/%s"%postID)
        else:
            error = "We need both a title and an entry!"
            self.render("newentry.html", subject=subject, content=content, error=error)
            

class AllEntries(Handler):        
    def get(self):
        entries, age = get_last10_age()
        user_id_hash = self.request.cookies.get('user_id')
        if check_secure_val(user_id_hash):
            username = get_username_from_id(user_id_hash.split('|')[0])
        else:
            username = None 
        self.render('front.html', entries = entries, age = age, user=username)

class OnePostJson(Handler):
    def get(self, postID):
        import json
        self.response.headers.add_header('content-type','application/json')
        self.response.headers.add_header('charset','UTF-8')
        if memcache.get(postID) == None:
            key = db.Key.from_path('Entry', int(postID))
            post = db.get(key)
            postTime = time.time()
            memcache.set(postID, (post, postTime))
        (post, postTime) = memcache.get(postID)        
        if not post:
            self.error(404)
            return
        bbhandler = createMyBBhandler()
        jsonPost = {"content":bbhandler(post.content), "subject":post.subject, "created":post.timestamp.strftime('%d %b %Y at %H:%M')}
        myjson = json.dumps(jsonPost)
        self.write(myjson)
        
            
class AllEntriesJson(Handler):        
    def get(self):
        import json
        self.response.headers.add_header('content-type','application/json')
        self.response.headers.add_header('charset','UTF-8')
        entries, age = get_last10_age()
        myjsonlist = []
        bbhandler = createMyBBhandler()
        bbhandler.add_tag(YoutubeTag, u'yt')
        for entry in entries:
            myjsondict = {"content":bbhandler(entry.content), "subject":entry.subject, "created":entry.timestamp.strftime('%d %b %Y at %H:%M')}
            myjsonlist.append(myjsondict)
        myjson = json.dumps(myjsonlist);
        self.write(myjson)
        
class RSSfeed(Handler):        
    def get(self):
        self.response.headers.add_header('content-type','application/rss+xml')
        self.response.headers.add_header('charset','UTF-8')
        import datetime
        import PyRSS2Gen
        entries, age = get_last10_age()
        myitems = []
        bbhandler = createMyBBhandler()
        bbhandler.add_tag(YoutubeTag, u'yt')
        for entry in entries:
            entryRss = PyRSS2Gen.RSSItem(
                             title = entry.subject,
                             link = "http://creepyweegirl.appspot.com/%s" % str(entry.key().id()),
                             description = bbhandler(entry.content),
                             guid = PyRSS2Gen.Guid("http://creepyweegirl.appspot.com/%s" % str(entry.key().id())),
                             pubDate = entry.timestamp)
            myitems.append(entryRss)
        rss = PyRSS2Gen.RSS2(
                        title = "Creepy Blog feed",
                        link = "http://creepyweegirl.appspot.com/",
                        description = "This blog exits mostly for the purpose of practising webapp development (feedback and suggestions on how to imporove it are welcome). However, it still is my blog and I will occasionally make posts about what I'm up to :)",
                        lastBuildDate = datetime.datetime.now(),
                        items = myitems)
        self.write(rss.to_xml(encoding="UTF-8"))
        
app = webapp2.WSGIApplication([(r'/newpost/?', PostNew), (r'/(\d+)',OnePost), (r'/edit/(\d+)',EditPost), (r'/?', AllEntries), (r'/(\d+)\.json',OnePostJson), (r'/\.json', AllEntriesJson), (r'/rss', RSSfeed)], debug=True)
