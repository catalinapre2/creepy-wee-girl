import os
import webapp2
import jinja2
import re
import logging
import postmarkup as pm

from jinja2 import evalcontextfilter, Markup, escape

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

_paragraph_re = re.compile(r'(?:\r\n|\r|\n){2,}')

#Filters---------------------------
@evalcontextfilter                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
def nl2br(eval_ctx, value):
    result = u'\n\n'.join(u'<p>%s</p>' % p.replace('\n', '<br>\n') \
        for p in _paragraph_re.split(escape(value)))
    if eval_ctx.autoescape:
        result = Markup(result)
    return result

jinja_env.filters['nl2br'] = nl2br

@evalcontextfilter
def bbCodeFilter(eval_ctx, value):
    bbhandler = createMyBBhandler()
    value = bbhandler(value)
    if eval_ctx.autoescape:
        value = Markup(value)
    return value

jinja_env.filters['bbCodeFilter'] = bbCodeFilter

#----------------------------------

def createMyBBhandler():
    bbhandler = pm.create(use_pygments=False)
    bbhandler.add_tag(YoutubeTag, u'yt')
    return bbhandler

class YoutubeTag(pm.TagBase):

    def __init__(self, name, **kwargs):
        super(YoutubeTag, self).__init__( name, inline=True)
        
    def open(self, parser, params, *args):
        if params.strip():
            self.auto_close = True            
        super(YoutubeTag, self).open(parser, params, *args)

    def render_open(self, parser, node_index):
        contents = self.get_contents(parser)
        self.skip_contents(parser)
        # Validate url to avoid any XSS attacks
        if self.params:
            videoID = self.params.strip()
        else:
            videoID = pm.strip_bbcode(contents)
        videoID = videoID.replace(u'"', u"%22").strip()
        if not videoID:
            return u''
        return u'<iframe width="560" height="315" src="http://www.youtube.com/embed/%s" frameborder="0" allowfullscreen></iframe>' % pm.PostMarkup.standard_replace_no_break(videoID)

#----------------------------------

class Handler(webapp2.RequestHandler):
	def write(self, *a, **kw):
		self.response.out.write(*a, **kw)
		
	def render_str(self, template, **params):
		t = jinja_env.get_template(template)
		return t.render(params)
		
	def render(self, template, **kw):
		self.write(self.render_str(template, **kw))
